package com.infoarmy.owler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.infoarmy.owler.apihandlers.LoginAPIHandler;
import com.infoarmy.owler.utils.EmailValidator;

public class LoginScreen extends Activity implements OnClickListener {

	/**
	 * Login Button
	 */
	private Button mLoginButton;
	/**
	 * Username EditText
	 */
	private EditText mUserName;
	/**
	 * Password EditText
	 */
	private EditText mPassword;
	/**
	 * Email validator
	 */
	private EmailValidator emailValidator = new EmailValidator();
	/**
	 * Login Api handler to validate login
	 */
	private LoginAPIHandler loginApiHandler = new LoginAPIHandler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_screen);
		initializeViews();
		setupListeners();

		/*mUserName.setText("ab@cd.com");
		mPassword.setText("1234");*/
	}

	/**
	 * Sets up the listeners to the view
	 */
	private void setupListeners() {
		mLoginButton.setOnClickListener(this);
		mUserName.setOnFocusChangeListener(new EmailValidate());
	}

	/**
	 * Initalizes the views
	 */
	private void initializeViews() {
		mLoginButton = (Button) findViewById(R.id.loginButton);
		mUserName = (EditText) findViewById(R.id.userName);
		mPassword = (EditText) findViewById(R.id.password);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.login_screen, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == R.id.loginButton) {
			if (validateViews())
				startActivity(new Intent(LoginScreen.this, FeedsScreen.class));
			else {
				ShakeAnimation();
				mPassword.setText("");
				mUserName.requestFocus();
			}
		}
	}

	/**
	 * Shake aniamtion, if login falis
	 */
	private void ShakeAnimation() {
		Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
		findViewById(R.id.password).startAnimation(shake);
		findViewById(R.id.userName).startAnimation(shake);
	}

	/**
	 * Validate the views
	 * 
	 * @return
	 */
	private boolean validateViews() {
		if (mUserName.getText().length() == 0) {
			Toast.makeText(LoginScreen.this,
					getString(R.string.enter_the_username), Toast.LENGTH_SHORT)
					.show();
			return false;
		} else if (!emailValidator.validate(mUserName.getText().toString())) {
			Toast.makeText(LoginScreen.this,
					getString(R.string.enter_the_username_valid_message),
					Toast.LENGTH_SHORT).show();
			return false;
		} else if (mPassword.getText().length() == 0) {
			Toast.makeText(LoginScreen.this,
					getString(R.string.enter_the_password), Toast.LENGTH_SHORT)
					.show();
			return false;

		}
		return loginApiHandler.login(mUserName.getText().toString(), mPassword
				.getText().toString());
	}

	/**
	 * Focus change listener
	 * 
	 * @author gautam
	 * 
	 */
	class EmailValidate implements OnFocusChangeListener {

		@Override
		public void onFocusChange(View arg0, boolean arg1) {
			if (!arg1) {
				if (!emailValidator.validate(mUserName.getText().toString()))
					mUserName.setError("Invalid Email !");
			}
		}
	}
}
