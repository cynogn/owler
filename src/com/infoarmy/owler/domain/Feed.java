package com.infoarmy.owler.domain;


public class Feed {
	private String url;
	private String feed;
	private String feedTime;

	Feed(String _url, String _feeds, String _feedsTime) {
		setUrl(_url);
		setFeed(_feeds);
		setFeedTime(_feedsTime);
	}

	Feed() {
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFeed() {
		return feed;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	public String getFeedTime() {
		return feedTime;
	}

	public void setFeedTime(String feedTime) {
		this.feedTime = feedTime;
	}

	@Override
	public String toString() {
		System.out.println("" + this.getUrl());
		System.out.println("" + this.getFeed());
		System.out.println("" + this.getFeedTime());
		return super.toString();
	}
}
