package com.infoarmy.owler.domain;

import java.util.ArrayList;

import com.infoarmy.owler.Constants;

public class Feeds {
	private ArrayList<Feed> feeds = new ArrayList<Feed>();

	public ArrayList<Feed> getFeeds() {
		return feeds;
	}

	public void setFeeds(ArrayList<Feed> feeds) {
		this.feeds = feeds;
	}

	public ArrayList<Feed> initalizeFeeds() {
		Feed feed = null;
		for (int i = 0; i < Constants.IMAGES.length; i++) {
			feed = new Feed(Constants.IMAGES[i], Constants.Tweets[i], (i + 1)
					+ " m");
			feeds.add(feed);
		}
		return this.getFeeds();

	}
}
