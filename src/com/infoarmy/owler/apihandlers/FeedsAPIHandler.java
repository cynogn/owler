package com.infoarmy.owler.apihandlers;

import com.infoarmy.owler.domain.Feeds;

public class FeedsAPIHandler {

	public Feeds getAllFeeds() {
		Feeds feeds = new Feeds();
		feeds.initalizeFeeds();
		return feeds;
	}

}
