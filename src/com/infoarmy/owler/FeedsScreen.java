package com.infoarmy.owler;

import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.EventLogTags.Description;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SlidingDrawer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.infoarmy.owler.adapters.FeedsListAdapter;
import com.infoarmy.owler.apihandlers.FeedsAPIHandler;

/**
 * @author Gautam {@link Description} FeedsScreen Activity displays the feeds in
 *         the screen
 * 
 */
public class FeedsScreen extends Activity implements OnClickListener {
	/**
	 * API Handler for feeds
	 */
	private FeedsAPIHandler feedsAPIHandler = new FeedsAPIHandler();
	/**
	 * Displays the feeds in the ListView
	 */
	private ListView mFeeds;
	/**
	 * Slider holds the survey
	 */
	@SuppressWarnings("deprecation")
	private SlidingDrawer mSurveySlider;
	/**
	 * Survey Holder holds the surveys
	 */
	private FrameLayout mSurveyHolder;
	/**
	 * infaltor to infalte the layouts dynamically
	 */
	private LayoutInflater mViewInflator;
	/**
	 * Copatitor Survey layout
	 */
	private View mCompatitorSurvey;
	/**
	 * Revenue Survey layout
	 */
	private View mReveneueSurvey;
	/**
	 * loutout survey Button
	 */
	private Button mLogout;
	/**
	 * Company Strength SUrvey
	 */
	private View mConpanyStrengthSurvey;
	/**
	 * Random Number generator
	 */
	private Random mRandomNumberGenererator = new Random();
	/**
*	 */
	private int mCredit = 0;
	/**
	 * Displays the total credits
	 */
	private TextView mCreditView;
	/**
	 * Low Revenue View
	 */
	private TextView mLowReveneueView;
	/**
	 * Medium reveneue View
	 */
	private TextView mMediumRevenueView;
	/**
	 * High revenue View
	 */
	private TextView mHighRevenueView;
	/**
	 * close button of reveneue
	 */
	private TextView mReveneueCloseButton;
	/**
	 * Feeds Filters
	 */
	private Spinner mFeedsFiler;
	/**
	 * Sliding handler to swipe up and swipe down
	 */
	private Button mSlidingHandler;

	/**
	 * Scroll change listener
	 */
	private ScrollChangeListener mScrollChangeListener = null;
	/**
	 * high strength View
	 */
	private TextView mHighStrengthView;
	/**
	 * low strength View
	 */
	private TextView mLowStrengthView;
	/**
	 * medium strength View
	 */
	private TextView strengthMediumView;
	/**
	 * Close button of the company strengh view
	 */
	private TextView strngthClose;
	/**
	 * Compatitor Yes button
	 */
	private Button mCompetitorYes;
	/**
	 * Compatitor No button
	 */
	private Button mCompetitorNo;
	/**
	 * compatitor close button
	 */
	private TextView mCompatitorClose;
	/**
	 * compatitor help button
	 */
	private TextView mCompatitorHelp;
	/**
	 * revwnue help button
	 */
	private TextView mReveneueHelp;
	/**
	 * strength Help button
	 */
	private TextView mStrengthHelp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feeds_screen);

		initializeViewsAndLoadValues();
		setupListeners();
		if (!checkInternetConnection())
			Toast.makeText(FeedsScreen.this, "No Internet Connection",
					Toast.LENGTH_SHORT).show();

	}

	private boolean checkInternetConnection() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifiNetwork = cm
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}

		NetworkInfo mobileNetwork = cm
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true;
		}

		return false;
	}

	private void setupListeners() {
		/*
		 * Listeners of the revenue layout
		 */

		mLowReveneueView.setOnClickListener(this);
		mMediumRevenueView.setOnClickListener(this);
		mHighRevenueView.setOnClickListener(this);
		mReveneueCloseButton.setOnClickListener(this);
		/*
		 * Listeners of the company strength layout
		 */
		mLowStrengthView.setOnClickListener(this);
		strengthMediumView.setOnClickListener(this);
		mHighStrengthView.setOnClickListener(this);
		strngthClose.setOnClickListener(this);
		/*
		 * Listeners for Compatetor layout
		 */
		mCompetitorYes.setOnClickListener(this);
		mCompetitorNo.setOnClickListener(this);

		mCompatitorClose.setOnClickListener(this);
		mStrengthHelp.setOnClickListener(this);
		mReveneueHelp.setOnClickListener(this);
		mCompatitorHelp.setOnClickListener(this);

		/**
		 * Slider listener for animate open and animate close
		 */
		mSlidingHandler.setOnClickListener(this);
		/**
		 * Loutout button
		 */
		mLogout.setOnClickListener(this);
		/**
		 * Feeds listview touch lisner to active scroll view listener
		 */
		mFeeds.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (mScrollChangeListener == null) {
					mScrollChangeListener = new ScrollChangeListener();
					mFeeds.setOnScrollListener(mScrollChangeListener);
				}
				return false;
			}
		});

		FeedsListAdapter adapter = new FeedsListAdapter(this,
				(feedsAPIHandler).getAllFeeds());
		mFeeds.setAdapter(adapter);
		ArrayList<String> feedsCategory = new ArrayList<String>();
		feedsCategory.add("All");
		feedsCategory.add("My Feeds");
		feedsCategory.add("Favorited");

		ArrayAdapter<String> adapter_status = new ArrayAdapter<String>(
				FeedsScreen.this, R.layout.spinner_textview, feedsCategory);
		adapter_status.setDropDownViewResource(R.layout.spinner_textview);

		mFeedsFiler.setAdapter(adapter_status);
	}

	private void displaySurvey() {
		mSurveyHolder.removeAllViews();
		int randNumber = generateRandomNumber();
		System.out.println(randNumber);
		if (randNumber == 1)
			mSurveyHolder.addView(mCompatitorSurvey);
		else if (randNumber == 2)
			mSurveyHolder.addView(mReveneueSurvey);
		else if (randNumber == 3)
			mSurveyHolder.addView(mConpanyStrengthSurvey);
	}

	/**
	 * Initaliszes the views in the layouts
	 */
	private void initializeViewsAndLoadValues() {
		mCredit = 10;
		mFeeds = (ListView) findViewById(R.id.feedsList);
		mSlidingHandler = (Button) findViewById(R.id.handle);
		mLogout = (Button) findViewById(R.id.logout);

		mViewInflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mSurveySlider = (SlidingDrawer) findViewById(R.id.surveySlider);
		mCreditView = (TextView) findViewById(R.id.credit);
		mSurveyHolder = (FrameLayout) findViewById(R.id.surveyFrame);
		mCompatitorSurvey = mViewInflator.inflate(R.layout.competitors_layout,
				null);
		mReveneueSurvey = mViewInflator.inflate(R.layout.revenue_layout, null);
		mConpanyStrengthSurvey = mViewInflator.inflate(
				R.layout.strength_layout, null);

		mLowReveneueView = (TextView) mReveneueSurvey
				.findViewById(R.id.reveneue_low);
		mMediumRevenueView = (TextView) mReveneueSurvey
				.findViewById(R.id.reveneue_medium);
		mHighRevenueView = (TextView) mReveneueSurvey
				.findViewById(R.id.reveneue_hight);
		mReveneueCloseButton = (TextView) mReveneueSurvey
				.findViewById(R.id.revenueCloseButton);

		mLowStrengthView = (TextView) mConpanyStrengthSurvey
				.findViewById(R.id.strength_low);
		strengthMediumView = (TextView) mConpanyStrengthSurvey
				.findViewById(R.id.strength_medium);
		mHighStrengthView = (TextView) mConpanyStrengthSurvey
				.findViewById(R.id.strength_high);
		strngthClose = (TextView) mConpanyStrengthSurvey
				.findViewById(R.id.strengthCloseButton);

		mCompetitorYes = (Button) mCompatitorSurvey
				.findViewById(R.id.competitorYes);
		mCompetitorNo = (Button) mCompatitorSurvey
				.findViewById(R.id.competitorNo);

		mCompatitorHelp = (TextView) mCompatitorSurvey
				.findViewById(R.id.competitorsHelp);
		mReveneueHelp = (TextView) mReveneueSurvey
				.findViewById(R.id.helpRevenueButton);
		mStrengthHelp = (TextView) mConpanyStrengthSurvey
				.findViewById(R.id.helpStrengthButton);

		mCompatitorClose = (TextView) mCompatitorSurvey
				.findViewById(R.id.competitorsClose);

		mFeedsFiler = (Spinner) findViewById(R.id.spinnerCategory);
	}

	/**
	 * Generates and random number from (1 to 3)
	 * 
	 * @return
	 */
	int generateRandomNumber() {
		return mRandomNumberGenererator.nextInt(3) + 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 * 
	 * on click any button this funtion will be called
	 */
	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == R.id.reveneue_low) {
			mCredit += 10;
			Toast.makeText(FeedsScreen.this, "Survey Completed!",
					Toast.LENGTH_SHORT).show();
			closeSurvey();
		} else if (arg0.getId() == R.id.reveneue_medium) {
			mCredit += 10;
			Toast.makeText(FeedsScreen.this, "Survey Completed!",
					Toast.LENGTH_SHORT).show();
			closeSurvey();
		} else if (arg0.getId() == R.id.reveneue_hight) {
			mCredit += 10;
			Toast.makeText(FeedsScreen.this, "Survey Completed!",
					Toast.LENGTH_SHORT).show();
			closeSurvey();
		} else if (arg0.getId() == R.id.revenueCloseButton) {
			closeSurvey();
		} else if (arg0.getId() == R.id.logout) {
			finish();
		} else if (arg0.getId() == R.id.strength_low) {
			mCredit += 10;
			Toast.makeText(FeedsScreen.this, "Survey Completed!",
					Toast.LENGTH_SHORT).show();
			closeSurvey();
		} else if (arg0.getId() == R.id.strength_medium) {
			mCredit += 10;
			Toast.makeText(FeedsScreen.this, "Survey Completed!",
					Toast.LENGTH_SHORT).show();
			closeSurvey();
		} else if (arg0.getId() == R.id.strength_high) {
			mCredit += 10;
			Toast.makeText(FeedsScreen.this, "Survey Completed!",
					Toast.LENGTH_SHORT).show();
			closeSurvey();
		} else if (arg0.getId() == R.id.strengthCloseButton) {
			closeSurvey();
		} else if (arg0.getId() == R.id.competitorYes) {
			mCredit += 10;
			Toast.makeText(FeedsScreen.this, "Survey Completed!",
					Toast.LENGTH_SHORT).show();
			closeSurvey();
		} else if (arg0.getId() == R.id.competitorNo) {
			mCredit += 10;
			Toast.makeText(FeedsScreen.this, "Survey Completed!",
					Toast.LENGTH_SHORT).show();
			closeSurvey();
		} else if (arg0.getId() == R.id.competitorsClose) {
			closeSurvey();
		} else if (arg0.getId() == R.id.helpRevenueButton) {
			Toast.makeText(FeedsScreen.this,
					"Answer this survey and earn points", Toast.LENGTH_SHORT)
					.show();
		} else if (arg0.getId() == R.id.helpStrengthButton) {
			Toast.makeText(FeedsScreen.this,
					"Answer this survey and earn points", Toast.LENGTH_SHORT)
					.show();
		} else if (arg0.getId() == R.id.competitorsHelp) {
			Toast.makeText(FeedsScreen.this,
					"Answer this survey and earn points", Toast.LENGTH_SHORT)
					.show();
		}

	}

	/**
	 * Aniamtes and closes the survey
	 */
	private void closeSurvey() {
		updateCredits();
		mSurveySlider.animateClose();
	}

	/**
	 * Updates the credits
	 */
	void updateCredits() {
		mCreditView.setText(String.valueOf(mCredit));

	}

	/**
	 * ScrollChangeListener Class
	 * 
	 * @author gautam
	 * 
	 *         {@link Description} On scroll anuiamtes and open the sliding
	 *         drawer
	 * 
	 */

	/* private boolean flag = false; */

	private boolean flag;

	class ScrollChangeListener implements OnScrollListener {

		/* private boolean flag = false; */

		@Override
		public void onScrollStateChanged(AbsListView arg0, int arg1) {
			switch (arg1) {
			case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL: {
				if (!mSurveySlider.isOpened()) {
					mSurveySlider.animateOpen();
					displaySurvey();
				}
			}

			default:
				break;
			}
		}

		@Override
		public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {

		}
	}
}
