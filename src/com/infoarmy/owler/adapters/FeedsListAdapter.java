package com.infoarmy.owler.adapters;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.infoarmy.owler.R;
import com.infoarmy.owler.domain.Feed;
import com.infoarmy.owler.domain.Feeds;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class FeedsListAdapter extends BaseAdapter {
	private Feeds feeds;
	private Activity activity;
	private final LayoutInflater mInflater;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private ImageLoader imageLoader;

	/*
	 * private DisplayImageOptions options;
	 */
	public FeedsListAdapter(Activity a, Feeds _feeds) {
		this.activity = a;
		mInflater = LayoutInflater.from(activity.getApplicationContext());
		this.feeds = _feeds;
		imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				activity).threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.LIFO).enableLogging()
				.build();
		imageLoader.init(config);

	}

	private class ViewHolder {
		public TextView text;
		public ImageView image;
		public TextView timestamp;
	}

	@Override
	public int getCount() {
		return feeds.getFeeds().size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		final ViewHolder holder;
		if (convertView == null) {
			view = activity.getLayoutInflater().inflate(
					R.layout.feeds_list_row, parent, false);
			holder = new ViewHolder();
			holder.text = (TextView) view.findViewById(R.id.text);
			holder.image = (ImageView) view.findViewById(R.id.image);
			holder.timestamp = (TextView) view.findViewById(R.id.timeStamp);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		Feed feed = feeds.getFeeds().get(position);
		holder.text.setText(feed.getFeed());
		holder.timestamp.setText(feed.getFeedTime());
		imageLoader.displayImage(feed.getUrl(), holder.image,
				animateFirstListener);
		return view;
	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
				//	FadeInBitmapDisplayer.animate(imageView, 300);
					displayedImages.add(imageUri);
				}
			}
		}
	}
}